// This is the code to add the username to the welcoming message and show the user's current score

import {get, post} from './ajaxSampleScript.js'
import { increment } from './increment.js'

const displayed_username = document.querySelector('.displayed_username')
const fizzbuzz_value = document.querySelector('.fizzbuzz_value')

// object to store the score of the user
export const param = {
    score: 0
}

export const displayUsernameAndFizzBuzzValue = (username) => {
    displayed_username.innerHTML = username
    get(`http://basic-web.dev.avc.web.usf.edu/${username}`).then(function(response) {
        if (response.status == 200) {
            param.score = response.data.score
            if (response.data.score === 0) fizzbuzz_value.innerHTML = 0
            else if (response.data.score % 3 === 0 && response.data.score % 5 === 0) fizzbuzz_value.innerHTML = 'FizzBuzz'
            else if (response.data.score % 3 === 0) fizzbuzz_value.innerHTML = 'Fizz'
            else if (response.data.score % 5 === 0) fizzbuzz_value.innerHTML = 'Buzz'
            else fizzbuzz_value.innerHTML = response.data.score
        } else {
            post(`http://basic-web.dev.avc.web.usf.edu/${username}`, {score: 0}).then(function(response) {
                switch(response.status) {
                    case 400:
                        console.error(response.data);
                        break;
                    case 500:
                        console.error(response.data);
                        break;
                }
            })
            fizzbuzz_value.innerHTML = 0
        }
    })

    // Start the function increment() right after the username and the score of the user is obtained
    increment()
}