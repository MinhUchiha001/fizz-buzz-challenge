// This is the code that handles log in activity

import { displayUsernameAndFizzBuzzValue } from "./display.js"
// import { getScore } from "./handleAPI.js"
import { increment } from "./increment.js"
import { get, post } from "./ajaxSampleScript.js"

// Get some elements inside the log in page
const entered_username = document.querySelector('.entered_username')
const login_page = document.querySelector('.login_page')
const btn_login = document.querySelector('.btn_login')

export const handleLogin = () => {
    // When btn_login is clicked, the name of the user will be added to the welcoming message in the main page and 
    // the login_page will move down to show the main page
    btn_login.addEventListener('click', () => {
        login_page.classList.add('finished')
        displayUsernameAndFizzBuzzValue (entered_username.value)
    })
}