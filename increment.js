// This is the code to handle event for the btn_increment button

import { post, get } from "./ajaxSampleScript.js";

const fizzbuzz_value = document.querySelector('.fizzbuzz_value')
const btn_increment = document.querySelector('.btn_increment')
const entered_username = document.querySelector('.entered_username')

import { param } from "./display.js";

// Function to increment the score everytime a user clicks the button
export const increment = () => {
    btn_increment.addEventListener('click', () => {
        param.score ++
        console.log(param.score);
        if (param.score === 0) fizzbuzz_value.innerText = 0
        else if (param.score % 3 === 0 && param.score % 5 === 0) fizzbuzz_value.innerText = 'FizzBuzz'
        else if (param.score % 3 === 0) fizzbuzz_value.innerText = 'Fizz'
        else if (param.score % 5 === 0) fizzbuzz_value.innerText = 'Buzz'
        else fizzbuzz_value.innerText = param.score

        // Update score in the database every time it increments
        post(`http://basic-web.dev.avc.web.usf.edu/${entered_username.value}`, {score: param.score}).then(function(response) {
            switch(response.status) {
                case 400:
                    console.error(response.data)
                    break
                case 500:
                    console.error(response.data)
                    break
            }
        })
    })                  
}